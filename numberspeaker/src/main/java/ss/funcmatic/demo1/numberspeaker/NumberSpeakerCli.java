package ss.funcmatic.demo1.numberspeaker;

public class NumberSpeakerCli {

    /**
     * Application entry point.
     */
    public static void main(String[] args) {
        try {
            int number = Integer.parseInt(args[0]);
            NumberSpeaker speaker = new RussianNumberSpeaker();
            System.out.println(speaker.speak(number));

        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.out.println("Invalid input.");
            printHelp();
        }
    }

    static void printHelp() {
        System.out.println("\nPlease provide an integer value:");
        System.out.println("\t<number>");
        System.out.println("Example:");
        System.out.println("\t1234");
    }
}
