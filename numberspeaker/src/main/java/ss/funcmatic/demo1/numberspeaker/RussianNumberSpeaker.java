package ss.funcmatic.demo1.numberspeaker;

import java.util.LinkedList;
import java.util.Objects;

public class RussianNumberSpeaker implements NumberSpeaker {
    private static final int FEMININE = 3;
    private static final int SINGLES = 10;
    private static final int TEENS = 20;
    private static final int TENS = 100;
    private static final int HUNDREDS = 1000;
    private String minus = "минус ";
    private String zero = "ноль";
    private String[] feminineSingles = {null, "одна", "две"}; // used to name thousands
    private String[] singles = {null,
            "один",
            "два",
            "три",
            "четыре",
            "пять",
            "шесть",
            "семь",
            "восемь",
            "девять",
            "десять",
            "одинадцать",
            "двенадцать",
            "тринадцать",
            "четырнадцать",
            "пятнадцать",
            "шестнадцать",
            "семнадцать",
            "восемнадцать",
            "девятнадцать"};

    private String[] tens = {null,
            null,
            "двадцать",
            "тридцать",
            "сорок",
            "пятьдесят",
            "шестьдесят",
            "семьдесят",
            "восемьдесят",
            "девяносто"};

    private String[] hundreds = {null,
            "сто",
            "двести",
            "триста",
            "четыреста",
            "пятьсот",
            "шестьсот",
            "семьсот",
            "восемьсот",
            "девятьсот"};

    private String[][] suffix = {{null, null, null},
            {"тысяча", "тысячи", "тысяч"},
            {"миллион", "миллиона", "миллионов"},
            {"миллиард", "миллиарда", "миллиардов"}};

    /**
     * Returns text representation of specified integer value.
     */
    @Override
    public String speak(int number) {
        String result = "";

        if (number == 0) {  // check zero value
            result = zero;
        } else {
            if (number < 0) {   // check negative value
                result = minus;
                number = Math.abs(number);
            }
            result += speakPositiveInteger(number);
        }
        return result;
    }

    /**
     * Returns text representation of specified positive integer value.
     */
    private String speakPositiveInteger(int number) {
        int counter = 0;
        LinkedList<String> words = new LinkedList<>();
        while (number != 0) {
            if ((number % TENS) < TEENS) {  // teens and less
                if ((number % HUNDREDS) != 0) {
                    words.addFirst(getSuffix(number % TEENS, counter));
                }
                words.addFirst(getSingles(number % TEENS, counter));
            } else {    // tens
                if ((number % HUNDREDS) != 0) {
                    words.addFirst(getSuffix(number % SINGLES, counter));
                }
                words.addFirst(getSingles(number % SINGLES, counter));
                words.addFirst(tens[(number % TENS) / SINGLES]);
            }
            // hundreds
            words.addFirst(hundreds[(number % HUNDREDS) / TENS]);

            number /= HUNDREDS;
            counter++;
        }
        words.removeIf(Objects::isNull);
        return String.join(" ", words);
    }

    private String getSuffix(int number, int counter) {
        switch (number) {
            case 1:
                return suffix[counter][0];
            case 2:     // fall through
            case 3:     // fall through
            case 4:
                return suffix[counter][1];
            default:
                return suffix[counter][2];
        }
    }

    private String getSingles(int number, int counter) {
        if (counter == 1 && (number < FEMININE)) {
            return feminineSingles[number];
        } else {
            return singles[number];
        }
    }
}