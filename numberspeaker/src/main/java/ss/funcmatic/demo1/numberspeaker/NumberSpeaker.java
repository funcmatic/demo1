package ss.funcmatic.demo1.numberspeaker;

public interface NumberSpeaker {

    String speak(int number);
}
