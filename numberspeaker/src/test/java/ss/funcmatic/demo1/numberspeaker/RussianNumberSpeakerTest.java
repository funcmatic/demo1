package ss.funcmatic.demo1.numberspeaker;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class RussianNumberSpeakerTest {

    NumberSpeaker sut = new RussianNumberSpeaker();

    @Test
    void speakZero() {
        // given
        String expected = "ноль";
        // when
        String actual = sut.speak(0);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speakOne() {
        // given
        String expected = "один";
        // when
        String actual = sut.speak(1);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak10() {
        // given
        String expected = "десять";
        // when
        String actual = sut.speak(10);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak19() {
        // given
        String expected = "девятнадцать";
        // when
        String actual = sut.speak(19);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak20() {
        // given
        String expected = "двадцать";
        // when
        String actual = sut.speak(20);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak31() {
        // given
        String expected = "тридцать один";
        // when
        String actual = sut.speak(31);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak61() {
        // given
        String expected = "шестьдесят один";
        // when
        String actual = sut.speak(61);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak99() {
        // given
        String expected = "девяносто девять";
        // when
        String actual = sut.speak(99);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak100() {
        // given
        String expected = "сто";
        // when
        String actual = sut.speak(100);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak201() {
        // given
        String expected = "двести один";
        // when
        String actual = sut.speak(201);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak310() {
        // given
        String expected = "триста десять";
        // when
        String actual = sut.speak(310);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak419() {
        // given
        String expected = "четыреста девятнадцать";
        // when
        String actual = sut.speak(419);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak520() {
        // given
        String expected = "пятьсот двадцать";
        // when
        String actual = sut.speak(520);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak731() {
        // given
        String expected = "семьсот тридцать один";
        // when
        String actual = sut.speak(731);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak999() {
        // given
        String expected = "девятьсот девяносто девять";
        // when
        String actual = sut.speak(999);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak1000() {
        // given
        String expected = "одна тысяча";
        // when
        String actual = sut.speak(1000);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak2000() {
        // given
        String expected = "две тысячи";
        // when
        String actual = sut.speak(2000);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak5000() {
        // given
        String expected = "пять тысяч";
        // when
        String actual = sut.speak(5000);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak10_000() {
        // given
        String expected = "десять тысяч";
        // when
        String actual = sut.speak(10_000);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak11_000() {
        // given
        String expected = "одинадцать тысяч";
        // when
        String actual = sut.speak(11_000);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak12_000() {
        // given
        String expected = "двенадцать тысяч";
        // when
        String actual = sut.speak(12_000);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak20_000() {
        // given
        String expected = "двадцать тысяч";
        // when
        String actual = sut.speak(20_000);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak21_000() {
        // given
        String expected = "двадцать одна тысяча";
        // when
        String actual = sut.speak(21_000);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak22_020() {
        // given
        String expected = "двадцать две тысячи двадцать";
        // when
        String actual = sut.speak(22_020);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak32300() {
        // given
        String expected = "тридцать две тысячи триста";
        // when
        String actual = sut.speak(32300);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak300_030() {
        // given
        String expected = "триста тысяч тридцать";
        // when
        String actual = sut.speak(300_030);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak999_999() {
        // given
        String expected = "девятьсот девяносто девять тысяч девятьсот девяносто девять";
        // when
        String actual = sut.speak(999_999);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak_1_000_000() {
        // given
        String expected = "один миллион";
        // when
        String actual = sut.speak(1_000_000);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak_2_000_200() {
        // given
        String expected = "два миллиона двести";
        // when
        String actual = sut.speak(2_000_200);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak_2_000_000() {
        // given
        String expected = "два миллиона двести";
        // when
        String actual = sut.speak(2_000_200);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak_4_041_401() {
        // given
        String expected = "четыре миллиона сорок одна тысяча четыреста один";
        // when
        String actual = sut.speak(4_041_401);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak_5_500_552() {
        // given
        String expected = "пять миллионов пятьсот тысяч пятьсот пятьдесят два";
        // when
        String actual = sut.speak(5_500_552);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speak_2_147_483_647() {
        // given
        String expected = "два миллиарда сто сорок семь миллионов четыреста восемьдесят три тысячи шестьсот сорок семь";
        // when
        String actual = sut.speak(2_147_483_647);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speakNegative_2_147_483_647() {
        // given
        String expected = "минус два миллиарда сто сорок семь миллионов четыреста восемьдесят три тысячи шестьсот сорок семь";
        // when
        String actual = sut.speak(-2_147_483_647);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speakNegativeZeroAsZero() {
        // given
        String expected = "ноль";
        // when
        String actual = sut.speak(-0);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speakNegative_1_234_456_789() {
        // given
        String expected = "минус один миллиард двести тридцать четыре миллиона четыреста пятьдесят шесть тысяч семьсот восемьдесят девять";
        // when
        String actual = sut.speak(-1_234_456_789);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void speakIntegerMinValueInvokesException() {
        assertThrows(IndexOutOfBoundsException.class, () -> sut.speak(Integer.MIN_VALUE));
    }
}