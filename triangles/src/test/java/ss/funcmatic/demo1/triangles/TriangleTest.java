package ss.funcmatic.demo1.triangles;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

class TriangleTest {

    // ******* isPossibleTriangle tests  *******
    @Test
    void createImpossibleTriangleInvokesIllegalArgumentException() {
        // when
        Executable actual = () ->  Triangle.createTriangle("", 1.0d, 2.0d, 3.0d);
        // then
        assertThrows(IllegalArgumentException.class, actual);
    }

    @Test
    void createTriangleCreatesTriangleWithSpecifiedName() {
        // given
        String expected = "specific name";
        // when
        Triangle sut = Triangle.createTriangle("specific name", 2.0d, 2.0d, 3.0d);
        String actual = sut.getName();
        // then
        assertEquals(expected, actual);
    }

    @Test
    void createTriangleCreatesTriangleWithProperArea() {
        // given
        double expected = 77.071d;
        // when
        Triangle sut = Triangle.createTriangle("specific name", 12.0d, 13.0d, 19.0d);
        double actual = sut.getArea();
        // then
        assertEquals(expected, actual, 0.001d);
    }

    // ******* isPossibleTriangle tests  *******
    @Test
    void trueOnPossibleTriangle() {
        // when
        boolean actual = Triangle.isPossibleTriangle(1.0d, 1.0d,1.0d);
        // then
        assertTrue(actual);
    }

    @Test
    void falseOnImpossibleTriangle() {
        // when
        boolean actual = Triangle.isPossibleTriangle(2.0d, 1.0d,1.0d);
        // then
        assertFalse(actual);
    }

    @Test
    void falseOnNegativeSides() {
        // when
        boolean actual = Triangle.isPossibleTriangle(-2.0d, -2.0d,-2.0d);
        // then
        assertFalse(actual);
    }

    // ******* getAreaUsingSides tests *******
    @Test
    void getAreaOfEquilateralTriangle() {
        // given
        double expected = 0.433d;
        // when
        double actual = Triangle.getAreaUsingSides(1.0d, 1.0d, 1.0d);
        // then
        assertEquals(expected, actual, 0.001d);
    }

    @Test
    void getAreaOfIsoscelesTriangle() {
        // given
        double expected = 7.154d;
        // when
        double actual = Triangle.getAreaUsingSides(5.0d, 3.0d, 5.0d);
        // then
        assertEquals(expected, actual, 0.001d);
    }

    @Test
    void getAreaOfImpossibleTriangle() {
        // when
        Executable actual = () ->
            Triangle.getAreaUsingSides(3.0d, 1.0d, 1.0d);
        // then
        assertThrows(IllegalArgumentException.class, actual);
    }

    // ******* compareTo tests  *******
    @Test
    void compareSmallerTriangleToBiggerTriangle() {
        // given
        Triangle t1 = Triangle.createTriangle("one", 2,2,2);
        Triangle t2 = Triangle.createTriangle("two", 3,3,3);
        int expected = -1;
        // when
        int actual = t1.compareTo(t2);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void compareBiggerTriangleToSmallerTriangle() {
        // given
        Triangle t1 = Triangle.createTriangle("one", 10,12,10);
        Triangle t2 = Triangle.createTriangle("two", 3,3,2);
        int expected = 1;
        // when
        int actual = t1.compareTo(t2);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void compareSameTriangle() {
        // given
        Triangle t1 = Triangle.createTriangle("one", 2,3,4);
        int expected = 0;
        // when
        int actual = t1.compareTo(t1);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void compareDifferentTriangleWithSameArea() {
        // given
        Triangle t1 = Triangle.createTriangle("one", 2,3,4);
        Triangle t2 = Triangle.createTriangle("two", 3,4,2);
        int expected = 0;
        // when
        int actual = t1.compareTo(t2);
        // then
        assertEquals(expected, actual);
    }
}