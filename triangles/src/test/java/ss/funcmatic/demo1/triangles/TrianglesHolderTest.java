package ss.funcmatic.demo1.triangles;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TrianglesHolderTest {
    TrianglesHolder sut;

    @BeforeEach
    void beforeEach() {
        sut = new TrianglesHolder();
    }

    @Test
    void returnsOneLineOnZeroTrianglesAdded() {
        // given
        int expected = 1;
        // when
        int actual = sut.getFormattedOutputAsList().size();
        // then
        assertEquals(expected, actual);
    }

    @Test
    void returnsTwoLinesOnOneTriangleAdded() {
        // given
        int expected = 2;
        // when
        sut.addTriangle("one", 2, 3,4);
        int actual = sut.getFormattedOutputAsList().size();
        // then
        assertEquals(expected, actual);
    }

    @Test
    void returnsThreeLinesOnTwoTriangleAdded() {
        // given
        int expected = 3;
        // when
        sut.addTriangle("one", 2, 3,4);
        sut.addTriangle("two", 4, 3,4);
        int actual = sut.getFormattedOutputAsList().size();
        // then
        assertEquals(expected, actual);
    }

    @Test
    void returnsHeaderLineOnOneTriangleAdded() {
        // given
        String expected = "============= Triangles list: ===============";
        // when
        sut.addTriangle("one", 2, 3,4);
        String actual = sut.getFormattedOutputAsList().get(0);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void returnsProperFormatting() {
        // given
        String expected = "1. [Triangle one]: 2.905 cm";
        // when
        sut.addTriangle("one", 2, 3,4);
        String actual = sut.getFormattedOutputAsList().get(1);
        // then
        assertEquals(expected, actual);
    }
}