package ss.funcmatic.demo1.triangles;

public class Triangle implements Comparable<Triangle> {
    private String name;
    private double area;

    /**
     * Creates Triangle using name and 3 sides.
     */
    public static Triangle createTriangle(String name, double sideA, double sideB, double sideC) {
        return new Triangle(name, getAreaUsingSides(sideA, sideB, sideC));
    }

    /**
     * Checks if this triangle is possible.
     */
    public static boolean isPossibleTriangle(double sideA, double sideB, double sideC) {
        return     sideA < sideB + sideC
                && sideB < sideC + sideA
                && sideC < sideA + sideB;
    }

    public static double getAreaUsingSides(double sideA, double sideB, double sideC) {
        if (!isPossibleTriangle(sideA, sideB, sideC)) {
            throw new IllegalArgumentException("Not a triangle");
        }
        double p = (sideA + sideB + sideC) / 2.0d;  // semiperimeter
        return Math.sqrt(p * (p - sideA) * (p - sideB) * (p - sideC));
    }

    private Triangle(String name, double area) {
        this.name = name;
        this.area = area;
    }

    public String getName() {
        return name;
    }

    public double getArea() {
        return area;
    }

    @Override
    public int compareTo(Triangle o) {
        return Double.compare(area, o.area);
    }
}
