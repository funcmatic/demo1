package ss.funcmatic.demo1.triangles;

import java.util.*;

public class TrianglesCli {
    public static final int NAME = 0;
    public static final int SIDE_A = 1;
    public static final int SIDE_B = 2;
    public static final int SIDE_C = 3;

    /**
     * Application entry point.
     *
     * @param args is ignored.
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        TrianglesHolder holder = new TrianglesHolder();
        String[] input;
        do {
            System.out.println("Add triangle using following pattern:"
                    + "\n\t<name>, <length of side a>, <length of side b>, <length of side c>");
            try {
                input = scan.nextLine().split(",");

                holder.addTriangle(input[NAME].trim(),
                        Double.parseDouble(input[SIDE_A]),
                        Double.parseDouble(input[SIDE_B]),
                        Double.parseDouble(input[SIDE_C]));

            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            } catch (Exception e) {
                System.out.println("Invalid input");
            }
        } while (confirm(scan, "Another one? (type 'y' or 'yes' to add triangle)"));

        holder.getFormattedOutputAsList().forEach(System.out::println);
    }

    /**
     * Creates simple confirm dialogue with the user.
     *
     * @param scan    command line interface scanner.
     * @param message message for the user.
     * @return true when user agrees.
     */
    private static boolean confirm(Scanner scan, String message) {
        System.out.println(message);
        String input = scan.nextLine().toLowerCase();
        return input.equals("y") || input.equals("yes");
    }
}