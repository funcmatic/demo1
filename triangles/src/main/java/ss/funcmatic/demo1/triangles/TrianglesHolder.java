package ss.funcmatic.demo1.triangles;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class TrianglesHolder {
    private List<Triangle> triangleList = new ArrayList<>();

    public void addTriangle(String name, double sideA, double sideB, double sideC) {
        Triangle triangle = Triangle.createTriangle(name.trim(), sideA, sideB, sideC);
        triangleList.add(triangle);
    }

    public List<String> getFormattedOutputAsList() {
        triangleList.sort(Comparator.reverseOrder());
        List<String> output = new ArrayList<>();
        if (triangleList.isEmpty()) {
            output.add("No triangles was added.");
        } else {
            DecimalFormat df = new DecimalFormat("#.###", new DecimalFormatSymbols(Locale.US));
            int i = 0;
            output.add("============= Triangles list: ===============");
            for (Triangle t : triangleList) {
                output.add(++i + ". [Triangle " + t.getName() + "]: " + df.format(t.getArea()) + " cm");
            }
        }
        return output;
    }
}