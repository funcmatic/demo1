package ss.funcmatic.demo1.envelopes;

import java.util.Scanner;

import static ss.funcmatic.demo1.envelopes.EnvelopeCombiner.*;

public class EnvelopesCli {

    /**
     * Application entry point.
     *
     * @param args are ignored.
     */
    public static void main(String[] args) {
        final Scanner scan = new Scanner(System.in);

        do {
            double length1 = parseSideLength(scan,
                    "Please, enter a length of the first envelope: ");
            double width1 = parseSideLength(scan,
                    "Please, enter a width of the first envelope: ");
            double length2 = parseSideLength(scan,
                    "Please, enter a length of the second envelope: ");
            double width2 = parseSideLength(scan,
                    "Please, enter a width of the second envelope: ");

            Envelope first = new Envelope(length1, width1);
            Envelope second = new Envelope(length2, width2);

            switch (getCombination(first, second)) {
                case FIRST_IN_SECOND:
                    System.out.println("The first envelope can be placed inside the second one.");
                    break;
                case SECOND_IN_FIRST:
                    System.out.println("The second envelope can be placed inside the first one.");
                    break;
                case NONE:
                    // fall through
                default:
                    System.out.println("None of the envelopes can be placed inside another.");
            }
        } while (confirm(scan, "\nAnother try? (type 'y' or 'yes' to repeat)"));
    }

    /**
     * Creates simple confirm dialogue with the user.
     *
     * @param scan    command line interface scanner.
     * @param message message for the user.
     * @return true when user agrees.
     */
    private static boolean confirm(Scanner scan, String message) {
        System.out.println(message);
        String input = scan.nextLine();
        return input.equalsIgnoreCase("y")
                || input.equalsIgnoreCase("yes");
    }

    /**
     * Repeatedly analyzes user input until a valid length value is found.
     *
     * @param scan    command line interface scanner.
     * @param message for the user.
     * @return valid length value.
     */
    private static double parseSideLength(Scanner scan, String message) {
        Double result;
        while (true) {
            System.out.println(message);
            try {
                result = Double.parseDouble(scan.nextLine());
                if (Envelope.isValidSideLength(result)) {
                    break;
                }
            } catch (NumberFormatException e) {
                // should be logged but ignored
            }
            System.out.println("Invalid input.");
        }
        return result;
    }
}
