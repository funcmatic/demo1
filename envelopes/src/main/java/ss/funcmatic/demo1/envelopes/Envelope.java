package ss.funcmatic.demo1.envelopes;

public class Envelope implements Comparable<Envelope> {
    private double length;
    private double width;

    /**
     * Checks if provided Double parameter is a valid length value.
     */
    public static boolean isValidSideLength(Double d) {
        return !((d < Double.MIN_NORMAL) || d.isInfinite() || d.isNaN());
    }

    public Envelope(double length, double width) {
        this.length = Math.max(length, width);
        this.width = Math.min(length, width);
    }

    /**
     * Checks if this envelope can carry specified envelope.
     *
     * @param content inner envelope.
     * @return true on success.
     */
    public boolean isAbleToContain(Envelope content) {
        return (length - content.length > 0.0d)
                && (width - content.width > 0.0d);
    }

    @Override
    public int compareTo(Envelope e) {
        return Double.compare(length * width - e.length * e.width, 0.0);
    }
}
