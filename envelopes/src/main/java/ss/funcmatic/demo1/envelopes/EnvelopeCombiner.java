package ss.funcmatic.demo1.envelopes;

public class EnvelopeCombiner {
    public enum Combination {
        NONE, FIRST_IN_SECOND, SECOND_IN_FIRST
    }

    /**
     * Finds a way to combine envelopes, if possible.
     *
     * @param first  envelope
     * @param second envelope
     * @return way of Combination
     */
    public static Combination getCombination(Envelope first, Envelope second) {
        Combination result = Combination.NONE;
        int difference = first.compareTo(second);
        if ((difference > 0) && first.isAbleToContain(second)) {
            result = Combination.SECOND_IN_FIRST;
        } else if ((difference < 0) && second.isAbleToContain(first)) {
            result = Combination.FIRST_IN_SECOND;
        }
        return result;
    }
}
