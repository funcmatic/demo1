package ss.funcmatic.demo1.envelopes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EnvelopeTest {

    // ********** isValidSideLength **********
    @Test
    void nanIsNotValidSideLength() {
        // when
        boolean actual = Envelope.isValidSideLength(Double.NaN);
        // then
        assertFalse(actual);
    }

    @Test
    void infinityIsNotValidSideLength() {
        // when
        boolean actual = Envelope.isValidSideLength(Double.POSITIVE_INFINITY);
        // then
        assertFalse(actual);
    }

    @Test
    void negativeValueIsNotValidSideLength() {
        // when
        boolean actual = Envelope.isValidSideLength(-0.1d);
        // then
        assertFalse(actual);
    }

    @Test
    void zeroIsNotValidSideLength() {
        // when
        boolean actual = Envelope.isValidSideLength(0.0d);
        // then
        assertFalse(actual);
    }

    @Test
    void positiveValueIsValidSideLength() {
        // when
        boolean actual = Envelope.isValidSideLength(0.1d);
        // then
        assertTrue(actual);
    }

    // ********** compareTo **********
    @Test
    void returnOneWhenSubjectIsBiggerThanArgument() {
        // given
        int expected = 1;
        Envelope sut = new Envelope(2, 4);
        Envelope argument = new Envelope(3, 1);
        // when
        int actual = sut.compareTo(argument);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void returnZeroWhenSubjectAndArgumentHaveSameArea() {
        // given
        int expected = 0;
        Envelope sut = new Envelope(2, 6);
        Envelope argument = new Envelope(3, 4);
        // when
        int actual = sut.compareTo(argument);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void returnMinusOneWhenSubjectIsSmallerThanArgument() {
        // given
        int expected = -1;
        Envelope sut = new Envelope(2, 5);
        Envelope argument = new Envelope(3, 4);
        // when
        int actual = sut.compareTo(argument);
        // then
        assertEquals(expected, actual);
    }

    // ********** isAbleToContain **********
    @Test
    void trueWhenSubjectIsBiggerThanArgument() {
        // given
        Envelope sut = new Envelope(2, 4);
        Envelope argument = new Envelope(1, 3);
        // when
        boolean actual = sut.isAbleToContain(argument);
        // then
        assertTrue(actual);
    }

    @Test
    void trueWhenSubjectIsBiggerThanRotatedArgument() {
        // given
        Envelope sut = new Envelope(2, 4);
        Envelope argument = new Envelope(3, 1);
        // when
        boolean actual = sut.isAbleToContain(argument);
        // then
        assertTrue(actual);
    }

    @Test
    void falseWhenSubjectAndArgumentHaveSameSize() {
        // given
        Envelope sut = new Envelope(3, 4);
        Envelope argument = new Envelope(3, 4);
        // when
        boolean actual = sut.isAbleToContain(argument);
        // then
        assertFalse(actual);
    }

    @Test
    void falseWhenSubjectIsSmallerThanArgument() {
        // given
        Envelope sut = new Envelope(2, 5);
        Envelope argument = new Envelope(3, 4);
        // when
        boolean actual = sut.isAbleToContain(argument);
        // then
        assertFalse(actual);
    }

}