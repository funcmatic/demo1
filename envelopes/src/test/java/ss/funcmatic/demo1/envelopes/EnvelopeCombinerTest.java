package ss.funcmatic.demo1.envelopes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import ss.funcmatic.demo1.envelopes.EnvelopeCombiner.Combination;

class EnvelopeCombinerTest {

    @Test
    void whenFirstBiggerThanSecondThanSecondInFirst() {
        // given
        Combination expected = Combination.SECOND_IN_FIRST;
        Envelope first = new Envelope(2, 4);
        Envelope second = new Envelope(1, 3);
        // when
        Combination actual = EnvelopeCombiner.getCombination(first, second);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void whenFirstBiggerThanRotatedSecondThanSecondInFirst() {
        // given
        Combination expected = Combination.SECOND_IN_FIRST;
        Envelope first = new Envelope(2, 4);
        Envelope second = new Envelope(3, 1);
        // when
        Combination actual = EnvelopeCombiner.getCombination(first, second);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void whenFirstSameSizeAsSecondThanNone() {
        // given
        Combination expected = Combination.NONE;
        Envelope first = new Envelope(3, 4);
        Envelope second = new Envelope(3, 4);
        // when
        Combination actual = EnvelopeCombiner.getCombination(first, second);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void whenFirstSameSizeAsRotatedSecondThanNone() {
        // given
        Combination expected = Combination.NONE;
        Envelope first = new Envelope(3, 4);
        Envelope second = new Envelope(4, 3);
        // when
        Combination actual = EnvelopeCombiner.getCombination(first, second);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void whenFirstSmallerThanSecondThanNone() {
        // given
        Combination expected = Combination.FIRST_IN_SECOND;
        Envelope first = new Envelope(2, 5);
        Envelope second = new Envelope(3, 6);
        // when
        Combination actual = EnvelopeCombiner.getCombination(first, second);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void whenFirstSmallerThanRotatedSecondThanNone() {
        // given
        Combination expected = Combination.FIRST_IN_SECOND;
        Envelope first = new Envelope(2, 5);
        Envelope second = new Envelope(6, 3);
        // when
        Combination actual = EnvelopeCombiner.getCombination(first, second);
        // then
        assertEquals(expected, actual);
    }
}