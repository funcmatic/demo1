package ss.funcmatic.demo1.fibonacci;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FibonacciTest {
    Fibonacci sut;

    @BeforeEach
    void beforeEach() {
        sut = new Fibonacci();
    }

    @Test
    void byLength1() {
        // given
        List<Integer> expected = Arrays.asList(0, 1, 1, 2, 3, 5, 8);
        // when
        List<Integer> actual = sut.byLength(1);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void negativeLengthInvokesException() {
        // when
        Executable actual = () -> sut.byLength(-1);
        // then
        assertThrows(IllegalArgumentException.class, actual);
    }

    @Test
    void byLimitsMin0Max15() {
        // given
        List<Integer> expected = Arrays.asList(0, 1, 1, 2, 3, 5, 8, 13);
        // when
        List<Integer> actual = sut.byLimits(0, 15);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void wrongLimitsInvokesException() {
        // when
        Executable actual = () -> sut.byLimits(-1 , 5);
        // then
        assertThrows(IllegalArgumentException.class, actual);
    }
}