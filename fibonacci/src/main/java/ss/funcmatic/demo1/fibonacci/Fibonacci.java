package ss.funcmatic.demo1.fibonacci;

import java.util.ArrayList;
import java.util.List;

public class Fibonacci {
    private boolean switcher = false;
    private int first = 0;
    private int second = 1;

    /**
     * Returns next Fibonacci number.
     * Starts from 3rd element of common fibonacci row: 0, 1, 1, 2, 3, 5 and so on.
     */
    private int next() {
        return (switcher ^= true)
                ? (first += second)
                : (second += first);
    }

    /**
     * Returns a list of Fibonacci numbers that correspond to the specified length.
     *
     * @param length length of desired numbers.
     * @return list of Fibonacci numbers.
     * @throws IllegalArgumentException if length is zero or less
     */
    public List<Integer> byLength(int length) {
        if (length < 1) {
            throw new IllegalArgumentException("Length should not be 0 or less.");
        }
        List<Integer> result = new ArrayList<>();
        if (length == 1) {
            result.add(0);
            result.add(1);
        }

        int value = 0;
        int valueLength = 0;

        while (valueLength <= length) {
            valueLength = (int) Math.log10(value) + 1;
            if (valueLength == length) {
                result.add(value);
            }
            value = next();
        }
        return result;
    }

    /**
     * Returns a list of Fibonacci numbers that correspond to the specified range.
     * (Including upper and lower limits)
     *
     * @param minimum lower limit.
     * @param maximum upper limit.
     * @return a list that contains part of Fibonacci sequence
     * @throws IllegalArgumentException if range is not valid
     */
    public List<Integer> byLimits(int minimum, int maximum) {
        if (isNotValidLimits(minimum, maximum)) {
            throw new IllegalArgumentException("Range is not valid");
        }
        List<Integer> result = new ArrayList<>();
        if (minimum <= 0) {
            result.add(0);
        }
        int value = 1;
        while (value <= maximum) {
            if (value >= minimum) {
                result.add(value);
            }
            value = next();
        }
        return result;
    }

    private boolean isNotValidLimits(int min, int max) {
        return min > max
                || min < 0;
    }
}
