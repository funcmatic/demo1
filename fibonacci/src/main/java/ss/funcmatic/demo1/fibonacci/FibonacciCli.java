package ss.funcmatic.demo1.fibonacci;

public class FibonacciCli {

    /**
     * Application entry point.
     * Processes user input and executes corresponding method.
     */
    public static void main(String[] args) {
        Fibonacci f = new Fibonacci();
        try {
            switch (args.length) {
                case 1:
                    f.byLength(Integer.parseInt(args[0]))
                            .forEach(System.out::println);
                    break;
                case 2:
                    f.byLimits(Integer.parseInt(args[0]), Integer.parseInt(args[1]))
                            .forEach(System.out::println);
                    break;
                default:
                    throw new Exception("Invalid input.");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            printHelp();
        }
    }

    static void printHelp() {
        System.out.println("\nProvide a length (single argument) or a range (2 arguments)");
        System.out.println("Usage:");
        System.out.println("\t<length>  or  <minimum> <maximum>");
        System.out.println("Example:");
        System.out.println("\t6  or  10 2000");
    }
}