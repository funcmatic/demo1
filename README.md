# Demo1 repository
Anton Rybak

## [Task 1: Chessboard](chessboard/)

## [Task 2: Envelopes](envelopes/)

## [Task 3: Triangles](triangles/)

## [Task 4: File Parser](fileparser/)

## [Task 5: Number Speaker](numberspeaker/)

## [Task 6: Lucky Tickets](luckytickets/)

## [Task 7: Number Sequence](number_sequence/)

## [Task 8: Fibonacci Sequence](fibonacci/)