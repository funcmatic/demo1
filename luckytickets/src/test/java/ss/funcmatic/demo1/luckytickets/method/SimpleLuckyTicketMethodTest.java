package ss.funcmatic.demo1.luckytickets.method;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleLuckyTicketMethodTest {
    LuckyTicketMethod sut;

    @BeforeEach
    void beforeEach() {
        sut = new SimpleLuckyTicketMethod();
    }

    @Test
    void ticket0IsLucky() {
        // when
        boolean actual = sut.isLuckyTicket(0);
        //then
        assertTrue(actual);
    }

    @Test
    void ticket120003IsLucky() {
        // when
        boolean actual = sut.isLuckyTicket(120_003);
        //then
        assertTrue(actual);
    }

    @Test
    void ticket099882IsLucky() {
        // when
        boolean actual = sut.isLuckyTicket(99_882);
        //then
        assertTrue(actual);
    }

    @Test
    void ticket555951IsLucky() {
        // when
        boolean actual = sut.isLuckyTicket(555_951);
        //then
        assertTrue(actual);
    }

    @Test
    void ticket111102IsLucky() {
        // when
        boolean actual = sut.isLuckyTicket(111_102);
        //then
        assertTrue(actual);
    }

    @Test
    void ticket111104IsNotLucky() {
        // when
        boolean actual = sut.isLuckyTicket(111_104);
        //then
        assertFalse(actual);
    }

    @Test
    void ticket555952IsNotLucky() {
        // when
        boolean actual = sut.isLuckyTicket(555_952);
        //then
        assertFalse(actual);
    }

    @Test
    void ticket100000IsNotLucky() {
        // when
        boolean actual = sut.isLuckyTicket(100_000);
        //then
        assertFalse(actual);
    }

    @Test
    void ticket131313IsNotLucky() {
        // when
        boolean actual = sut.isLuckyTicket(131_313);
        //then
        assertFalse(actual);
    }
}