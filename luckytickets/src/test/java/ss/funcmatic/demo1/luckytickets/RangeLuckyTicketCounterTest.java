package ss.funcmatic.demo1.luckytickets;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import ss.funcmatic.demo1.luckytickets.method.LuckyTicketMethod;

import static org.junit.jupiter.api.Assertions.*;

class RangeLuckyTicketCounterTest {

    // ********** isNotValidRange() **********
    @Test
    void falseOnValidRange() {
        // given
        int minimum = 0;
        int maximum = 999_999;
        // when
        boolean actual = RangeLuckyTicketCounter.isNotValidRange(minimum, maximum);
        // then
        assertFalse(actual);
    }

    @Test
    void trueOnMinBiggerThanMax() {
        // given
        int minimum = 100;
        int maximum = 10;
        // when
        boolean actual = RangeLuckyTicketCounter.isNotValidRange(minimum, maximum);
        // then
        assertTrue(actual);
    }

    @Test
    void falseOnMinSameAsMax() {
        // given
        int minimum = 123_456;
        int maximum = 123_456;
        // when
        boolean actual = RangeLuckyTicketCounter.isNotValidRange(minimum, maximum);
        // then
        assertFalse(actual);
    }

    @Test
    void trueOnMinLessThanZero() {
        // given
        int minimum = -1;
        int maximum = 123;
        // when
        boolean actual = RangeLuckyTicketCounter.isNotValidRange(minimum, maximum);
        // then
        assertTrue(actual);
    }

    @Test
    void trueOnMaxMoreThan999_999() {
        // given
        int minimum = 123;
        int maximum = 1_000_000;
        // when
        boolean actual = RangeLuckyTicketCounter.isNotValidRange(minimum, maximum);
        // then
        assertTrue(actual);
    }

    // ********** countLuckyTicketsOnRange() **********
    @Test
    void invalidRangeThrowsIllegalArgumentException() {
        // given
        int minimum = -1;
        int maximum = 1;
        // when
        Executable actual = () -> RangeLuckyTicketCounter.countLuckyTicketsOnRange(minimum, maximum, null);
        // then
        assertThrows(IllegalArgumentException.class, actual);
    }

    @Test
    void invalidRangeThrowsIllegalArgumentException2() {
        // given
        int minimum = 1;
        int maximum = 1_000_000;
        // when
        Executable actual = () -> RangeLuckyTicketCounter.countLuckyTicketsOnRange(minimum, maximum, null);
        // then
        assertThrows(IllegalArgumentException.class, actual);
    }

    @Test
    void countsAllLuckyTickets() {
        // given
        int expected = 1_000_000;
        int minimum = 0;
        int maximum = 999_999;
        LuckyTicketMethod stub = ticketNumber -> true;
        // when
        int actual = RangeLuckyTicketCounter.countLuckyTicketsOnRange(minimum, maximum, stub);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void countsOnlyLuckyTickets() {
        // given
        int expected = 0;
        int minimum = 0;
        int maximum = 999_999;
        LuckyTicketMethod stub = ticketNumber -> false;
        // when
        int actual = RangeLuckyTicketCounter.countLuckyTicketsOnRange(minimum, maximum, stub);
        // then
        assertEquals(expected, actual);
    }
}