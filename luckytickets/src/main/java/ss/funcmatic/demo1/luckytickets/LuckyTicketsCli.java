package ss.funcmatic.demo1.luckytickets;

import ss.funcmatic.demo1.luckytickets.method.ComplexLuckyTicketMethod;
import ss.funcmatic.demo1.luckytickets.method.SimpleLuckyTicketMethod;

import static ss.funcmatic.demo1.luckytickets.RangeLuckyTicketCounter.countLuckyTicketsOnRange;

public class LuckyTicketsCli {

    /**
     * Application entry point.
     *
     * @param args min and max
     */
    public static void main(String[] args) {
        try {
            int min = Integer.parseInt(args[0]);
            int max = Integer.parseInt(args[1]);

            int simple = countLuckyTicketsOnRange(min, max, new SimpleLuckyTicketMethod());
            int complex = countLuckyTicketsOnRange(min, max, new ComplexLuckyTicketMethod());

            if (simple > complex) {
                System.out.println("\nThe simple method has bigger number of lucky tickets in this range.");
            } else if (simple < complex) {
                System.out.println("\nThe complex method has bigger number of lucky tickets in this range.");
            } else {
                System.out.println("\nBoth methods has the same number of lucky tickets in this range.");
            }
            System.out.println("Simple: " + simple + " vs Complex: " + complex);

        } catch (IndexOutOfBoundsException | NumberFormatException e) {
            System.out.println("Invalid input. Please, provide two six-digit integers.");
            printHelp();
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            printHelp();
        }
    }

    static void printHelp() {
        System.out.println("\nPlease, provide a ticket range:");
        System.out.println("\t<min> <max>");
        System.out.println("Example:");
        System.out.println("\t0 999999");
    }
}
