package ss.funcmatic.demo1.luckytickets;

import ss.funcmatic.demo1.luckytickets.method.LuckyTicketMethod;

public class RangeLuckyTicketCounter {
    public static final int LOWER_LIMIT = 0;
    public static final int UPPER_LIMIT = 999_999;

    /**
     * Returns the amount of lucky tickets of the given range.
     *
     * @param min range start
     * @param max range end
     * @param method used to determine if ticket is lucky or not.
     * @return count of lucky tickets.
     */
    public static int countLuckyTicketsOnRange(int min, int max, LuckyTicketMethod method) {
        if (isNotValidRange(min, max)) {
            throw new IllegalArgumentException(min + " " + max + " range is invalid.");
        }
        int counter = 0;
        for (int i = min; i <= max; i++) {
            if (method.isLuckyTicket(i)) {
                counter++;
            }
        }
        return counter;
    }

    /**
     * Checks if given range is valid or not.
     *
     * @param min range start
     * @param max range end
     * @return true if range is NOT valid.
     */
    public static boolean isNotValidRange(int min, int max) {
        return min > max
                || min < LOWER_LIMIT
                || max > UPPER_LIMIT;
    }
}
