package ss.funcmatic.demo1.luckytickets.method;

public class ComplexLuckyTicketMethod implements LuckyTicketMethod {

    /**
     * Checks if given ticket number is lucky one.
     */
    @Override
    public boolean isLuckyTicket(int ticketNumber) {
        int even = 0;
        int odd = 0;
        do {
            if ((ticketNumber % 2) == 0) {
                even += ticketNumber % DECIMAL_DELIMITER;
            } else {
                odd += ticketNumber % DECIMAL_DELIMITER;
            }
            ticketNumber /= DECIMAL_DELIMITER;
        } while (ticketNumber > 0);

        return even == odd;
    }
}
