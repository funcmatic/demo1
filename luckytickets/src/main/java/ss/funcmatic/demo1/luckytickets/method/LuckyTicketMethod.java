package ss.funcmatic.demo1.luckytickets.method;

public interface LuckyTicketMethod {
    int DECIMAL_DELIMITER = 10;

    /**
     * Checks if given ticket number is lucky one.
     */
    boolean isLuckyTicket(int ticketNumber);
}
