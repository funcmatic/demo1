package ss.funcmatic.demo1.luckytickets.method;

public class SimpleLuckyTicketMethod implements LuckyTicketMethod {
    public static final int SIMPLE_METHOD_DELIMITER = 1000;

    /**
     * Checks if given ticket number is lucky one.
     */
    @Override
    public boolean isLuckyTicket(int ticketNumber) {
        return sumOfDigits(ticketNumber / SIMPLE_METHOD_DELIMITER)
                == sumOfDigits(ticketNumber % SIMPLE_METHOD_DELIMITER);
    }

    /**
     * Sums up every digit of this number.
     *
     * @return sum of digits.
     */
    int sumOfDigits(int i) {
        int sum = 0;
        while (i > 0) {
            sum += i % DECIMAL_DELIMITER;
            i /= DECIMAL_DELIMITER;
        }
        return sum;
    }
}
