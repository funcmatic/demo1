package ss.funcmatic.demo1.numbersequence;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;
import static ss.funcmatic.demo1.numbersequence.NumberSequence.*;

class NumberSequenceTest {

    @Test
    void sequenceLength1Square0() {
        // given
        int [] expected = {0};
        // when
        int[] actual = getSequence(1, 0);
        // then
        assertArrayEquals(expected, actual);
    }

    @Test
    void sequenceLength2Square1() {
        // given
        int [] expected = {1, 2};
        // when
        int[] actual = getSequence(2, 1);
        // then
        assertArrayEquals(expected, actual);
    }

    @Test
    void sequenceLength3Square120() {
        // given
        int [] expected = {11, 12, 13};
        // when
        int[] actual = getSequence(3, 120);
        // then
        assertArrayEquals(expected, actual);
    }

    @Test
    void sequenceLength4Square121() {
        // given
        int [] expected = {11, 12, 13, 14};
        // when
        int[] actual = getSequence(4, 121);
        // then
        assertArrayEquals(expected, actual);
    }

    @Test
    void sequenceLength1Square122() {
        // given
        int [] expected = {12};
        // when
        int[] actual = getSequence(1, 122);
        // then
        assertArrayEquals(expected, actual);
    }

    @Test
    void negativeMinimumSquareInvokesIllegalArgumentException() {
        // given
        Class expected = IllegalArgumentException.class;
        // when
        Executable actual = () -> getSequence(1, -1);
        // then
        assertThrows(expected, actual);
    }

    @Test
    void negativeLengthInvokesIllegalArgumentException() {
        // given
        Class expected = IllegalArgumentException.class;
        // when
        Executable actual = () -> getSequence(-1, 0);
        // then
        assertThrows(expected, actual);
    }

    @Test
    void zeroLengthInvokesIllegalArgumentException() {
        // given
        Class expected = IllegalArgumentException.class;
        // when
        Executable actual = () -> getSequence(0, 0);
        // then
        assertThrows(expected, actual);
    }

    // ----- joinSequence() tests -----

    @Test
    void printSequenceOfThreeElementsSeparatedByCommaAndWhitespace() {
        // given
        String expected = "1, 2, 3";
        int[] sequence = {1, 2, 3};
        String separator = ", ";
        // when
        String actual = joinSequence(sequence, separator);
        // then
        assertEquals(expected, actual);
    }

    @Test
    void printSequenceOfOneElementSeparatedByWhitespace() {
        // given
        String expected = "7";
        int[] sequence = {7};
        String separator = " ";
        // when
        String actual = joinSequence(sequence, separator);
        // then
        assertEquals(expected, actual);
    }
}