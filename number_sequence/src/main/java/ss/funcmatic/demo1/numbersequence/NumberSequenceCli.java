package ss.funcmatic.demo1.numbersequence;

import static ss.funcmatic.demo1.numbersequence.NumberSequence.*;

public class NumberSequenceCli {
    public static final String SEPARATOR = ", ";

    /**
     * Application entry point.
     * Provides basic CLI implementation.
     */
    public static void main(String[] args) {
        try {
            int length = Integer.parseInt(args[0]);
            int minimum = Integer.parseInt(args[1]);

            int[] sequence = getSequence(length, minimum);
            System.out.println(joinSequence(sequence, SEPARATOR));

        } catch (IndexOutOfBoundsException e) {
            printHelp();
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            printHelp();
        }
    }

    static void printHelp() {
        System.out.println("\nPlease provide a sequence length and minimal square value:");
        System.out.println("\t<length> <minimum>");
        System.out.println("Example:");
        System.out.println("\t10 36");
    }
}