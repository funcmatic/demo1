package ss.funcmatic.demo1.numbersequence;

import java.util.Arrays;
import java.util.stream.Collectors;

public class NumberSequence {

    /**
     * Returns a number sequence that consists of natural numbers
     * whose square is not less than the specified minimum.
     * Both arguments must be positive integers (or zero for minimal square)
     * otherwise throws IllegalArgumentException.
     *
     * @param length  of the sequence
     * @param minimum square of the first element
     * @return the sequence
     */
    public static int[] getSequence(int length, int minimum) {
        if (length < 1) {
            throw new IllegalArgumentException("The length must be positive integer.");
        }
        if (minimum < 0) {
            throw new IllegalArgumentException("The minimum square must be positive integer or zero.");
        }

        int[] result = new int[length];
        minimum = (int) Math.ceil(Math.sqrt(minimum));

        for (int i = 0; i < length; i++) {
            result[i] = (minimum + i);
        }

        return result;
    }

    /**
     * Joins sequence elements into String using specified separator.
     */
    public static String joinSequence(int[] sequence, String separator) {
        return Arrays.stream(sequence)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(separator));
    }
}
