package ss.funcmatic.demo1.chessboard;

public class Chessboard {
    private int height;
    private int width;

    /**
     * Returns a new chessboard object using specified dimensions or
     * throws an exception in case if dimensions are not valid.
     *
     * @param height of the chessboard
     * @param width  of a chessboard
     * @return a valid chessboard
     * @throws IllegalArgumentException if dimensions are invalid
     */
    public static Chessboard getChessboard(int height, int width) {
        if (isCorrectChessboardDimensions(height, width)) {
            throw new IllegalArgumentException("Chessboard dimensions is invalid");
        }
        return new Chessboard(height, width);
    }

    public static boolean isCorrectChessboardDimensions(int height, int width) {
        return height < 1 || width < 1;
    }

    public static boolean isWhiteCell(int row, int column) {
        return ((row % 2) ^ (column % 2)) == 1;
    }

    private Chessboard(int height, int width) {
        this.height = height;
        this.width = width;
    }

    /**
     * Returns a multiline representation of this chessboard
     * using specified markers for black and white cells.
     *
     * @param whiteCell marker
     * @param blackCell marker
     * @return a multiline String
     */
    public String drawChessboardAsMultilineString(String whiteCell, String blackCell) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                sb.append(isWhiteCell(i, j) ? whiteCell : blackCell);
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
