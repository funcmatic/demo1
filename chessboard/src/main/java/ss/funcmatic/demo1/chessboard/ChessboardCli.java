package ss.funcmatic.demo1.chessboard;

public class ChessboardCli {
    public static final String BLACK_CELL = "* ";
    public static final String WHITE_CELL = "  ";

    /**
     * Application entry point.
     */
    public static void main(String[] args) {
        try {
            int height = Integer.parseInt(args[0]);
            int width = Integer.parseInt(args[1]);
            Chessboard chessboard = Chessboard.getChessboard(height, width);
            System.out.println(chessboard.drawChessboardAsMultilineString(WHITE_CELL, BLACK_CELL));

        } catch (IndexOutOfBoundsException | NumberFormatException e) {
            System.out.println("Invalid input. Please, provide two integers.");
            printHelp();
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            printHelp();
        }
    }

    static void printHelp() {
        System.out.println("\nPlease provide chessboard dimensions using following pattern:");
        System.out.println("\t<height> <width>");
        System.out.println("Example:");
        System.out.println("\t4 8");
    }

}
