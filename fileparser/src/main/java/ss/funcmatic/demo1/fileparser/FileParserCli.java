package ss.funcmatic.demo1.fileparser;

import java.io.IOException;
import java.nio.file.InvalidPathException;

import static ss.funcmatic.demo1.fileparser.FileParser.*;

public class FileParserCli {
    private static final int FILEPATH = 0;
    private static final int STRING = 1;
    private static final int REPLACEMENT = 2;

    /**
     * Application entry point.
     */
    public static void main(String[] args) {
        try {
            String uri = args[FILEPATH];
            switch (args.length) {
                case 2:     // count
                    System.out.println("Number of occurrences: "
                            + count(fetch(uri), args[STRING]));
                    break;
                case 3:     // replace
                    commit(uri, replace(fetch(uri), args[STRING], args[REPLACEMENT]));
                    System.out.println("\nDone!");
                    break;
                default:
                    throw new IllegalArgumentException("Invalid input.");
            }
        } catch (IndexOutOfBoundsException e) {
            printHelp();
        } catch (InvalidPathException e) {
            System.out.println(e.getMessage());
            printHelp();
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            printHelp();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            printHelp();
        }
    }

    static void printHelp() {
        System.out.println("\nPlease provide arguments using following patterns:");
        System.out.println("\t<filepath> <string to be counted>");
        System.out.println("or");
        System.out.println("\t<filepath> <string to be replaced> <replacement string>");
        System.out.println("Example:");
        System.out.println("\t\"D:/somedir/somefile.txt\" \"some string\"");
        System.out.println("or");
        System.out.println("\t\"\\somedir\\somefile.txt\" \"string to be replaced\" \"replacement string\"");
    }
}