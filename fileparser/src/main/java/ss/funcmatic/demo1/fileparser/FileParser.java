package ss.funcmatic.demo1.fileparser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileParser {

    /**
     * Fetches lines from a file using specified uri.
     *
     * @param uri to the file
     * @return all lines from the file
     * @throws IOException if an I/O error occurs reading from the file.
     */
    public static List<String> fetch(String uri) throws IOException {
        return Files.readAllLines(getValidFilePath(uri));
    }

    /**
     * Overwrites specified file using specified lines.
     *
     * @param uri   to the files
     * @param lines lines to be written
     * @return Path to the file
     * @throws IOException if an I/O error occurs writing to the file.
     */
    public static Path commit(String uri, List<String> lines) throws IOException {
        return Files.write(getValidFilePath(uri), lines);
    }

    /**
     * Counts number of occurrences of the specified string in the list.
     * (Sums up number of occurrences in each string from the list.)
     *
     * @param lines  list of Strings
     * @param string to be counted
     * @return total number of occurrences
     */
    public static int count(List<String> lines, String string) {
        int result = 0;
        for (String line : lines) {
            for (int i = 0; i != -1; ) {
                i = line.indexOf(string, i);
                if (i != -1) {
                    result++;
                    i += string.length();
                }
            }
        }
        return result;
    }

    /**
     * Replaces every single occurrence of specified line in the list.
     * Does not change the original list.
     *
     * @param lines       list
     * @param string      to be replaced
     * @param replacement string
     * @return list with replacements
     */
    public static List<String> replace(List<String> lines, String string, String replacement) {
        List<String> newLines = new ArrayList<>(lines.size());
        for (String line : lines) {
            if (line.contains(string)) {
                line = line.replace(string, replacement);
            }
            newLines.add(line);
        }
        return newLines;
    }

    private static Path getValidFilePath(String uri) {
        Path path = Paths.get(uri).normalize().toAbsolutePath();
        if (Files.notExists(path) || !Files.isRegularFile(path)) {
            throw new IllegalArgumentException("Invalid file path.");
        }
        return path;
    }
}