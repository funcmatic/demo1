package ss.funcmatic.demo1.fileparser;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FileParserTest {
    // ********* count() *********
    @Test
    void count3SubstringsInLines() {
        // given
        List<String> lines = Arrays.asList("далее и далее и далее", "и далее и далее");
        int expected = 2;
        // when
        int actual = FileParser.count(lines, "далее и далее");
        // then
        assertEquals(expected, actual);
    }

    @Test
    void count4SubstringsInLines() {
        // given
        List<String> lines = Arrays.asList("aaaa", "aa");
        int expected = 3;
        // when
        int actual = FileParser.count(lines, "aa");
        // then
        assertEquals(expected, actual);
    }

    // ********* replace() *********
    @Test
    void replaceSubstringInLines() {
        // given
        List<String> lines = Arrays.asList("далее и далее и далее и далее и далее", "и далее и далее");
        List<String> expected = Arrays.asList("более или менее и более или менее и далее", "и более или менее");
        // when
        List<String> actual = FileParser.replace(lines, "далее и далее", "более или менее");
        // then
        assertEquals(expected, actual);
    }

    // ********* fetch() *********
    @Test
    void fetchInvalidFilePathInvokesIllegalArgumentException() {
        // when
        Executable actual = () -> FileParser.fetch("?#%^/");
        // then
        assertThrows(IllegalArgumentException.class, actual);
    }

    @Test
    void fetchNotExistingFileInvokesIllegalArgumentException() {
        // when
        Executable actual = () -> FileParser.fetch("notexistingfile.abc");
        // then
        assertThrows(IllegalArgumentException.class, actual);
    }

    // ********* commit() *********
    @Test
    void commitInvalidFilePathInvokesIllegalArgumentException() {
        // given
        List<String> ignoredLines = Arrays.asList("ignored", "lines");
        // when
        Executable actual = () -> FileParser.commit("?#%^/", ignoredLines);
        // then
        assertThrows(IllegalArgumentException.class, actual);
    }
}